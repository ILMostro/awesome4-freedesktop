PREFIX=$(HOME)/.config
OBJS=*.lua

install:
	mkdir -p $(PREFIX)/awesome/freedesktop
	cp -r /etc/xdg/awesome/rc.lua $(PREFIX)/awesome/
	cp -r $(OBJS) $(PREFIX)/awesome/freedesktop/
	sed -i.bak 's/-- {{{ Error/local freedesktop = require freedesktop\n-- {{{ Error/' $(PREFIX)/awesome/rc.lua
	sed -i.bak2 's/mymainmenu = awful.menu/mymainmenu = freedesktop.menu.build/' $(PREFIX)/awesome/rc.lua

